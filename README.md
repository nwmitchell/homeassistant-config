# Home Assistant Configuration
[Home Assistant](https://home-assistant.io/) is an open-source home automation platform running on Python 3, designed to track, control, and automate all devices at home.

This repository contains the majority of the configurations for my instance of Home Assistant, excluding any sensitive data.

## Extra Components
In addition to the configurations present in this repo, these extra components are also used. While those configurations aren't exposed here (yet), the following list will serve as a placeholder in the interim.
* Appdaemon - [GitHub](https://github.com/home-assistant/appdaemon) / [Docs](http://appdaemon.readthedocs.io/en/latest/)
* HaDashboard - [Home Assistant Docs](https://www.home-assistant.io/docs/ecosystem/hadashboard/) / [HaDashboard Docs](https://appdaemon.readthedocs.io/en/stable/DASHBOARD_INSTALL.html)
* Occusim - [GitHub](https://github.com/acockburn/occusim)

## Home Assistant Setup
Follow the [instructions](https://www.home-assistant.io/docs/installation/raspberry-pi/) provided on the Home Assistant website.

## Run Home Assistant as a systemd service
Create `/etc/systemd/system/homeassistant.service` with the following content:
```
[Unit]
Description=Home Assistant
After=network-online.target

[Service]
Type=simple
User=homeassistant
ExecStart=/srv/homeassistant/bin/hass -c "/home/homeassistant/.homeassistant"

[Install]
WantedBy=multi-user.target
```

Enable and start the service:
```
sudo systemctl enable homeassistant.service
sudo systemctl start homeassistant.service
```

## Razberry Z-Wave Board
The [Z-Wave.me RaZberry](https://z-wave.me/products/razberry/) is a daughter card for the Raspberry PI providing Z-Wave functionality. Utilizing this provides native Z-Wave support without the need for an external hub.

### Setup
Edit /boot/config.txt and add the following lines at the bottom of the file:
```
dtoverlay=disable-bt
enable_uart=1
```

Comment out the following line:
```
#dtoverlay=pi3-miniuart-bt
```

Run the `sudo raspi-config` command and enable the serial interface through the menu selections.

Install the necessary packages:
```
sudo apt install libudev-dev
```

Disable the bluetooth service:
```
sudo systemctl stop bluetooth.service
sudo systemctl disable bluetooth.service
```

Disable the hciuart service:
```
sudo systemctl stop hciuart.service
sudo systemctl disable hciuart.service
```

Mask the serial-getty@ttyAMA0 service:
```
sudo systemctl stop serial-getty@ttyAMA0.service
sudo systemctl mask serial-getty@ttyAMA0.service
```

Once these steps are completed, reboot the Raspberry PI.

The RaZberry board should now be accesible via `/dev/ttyAMA0`. Check the file permissions and ensure that the group is set to `dialout` and has read/write permissions:
```
sudo chgrp dialout /dev/ttyAMA0
sudo chmod g+rw /dev/ttyAMA0
```

The `homeassistant` user should already be a member of the `dialout` group.

Under the virtual environment for Home Assistant, install the `python-openzwave` package:
```
pip3 install python-openzwave
```

### Generate Encryption Key
To generate an encryption key, run the following command and drop the trailing comma:
```
cat /dev/urandom | tr -dc '0-9A-F' | fold -w 32 | head -n 1 | sed -e's/\(..\)/0x\1,/g'
```