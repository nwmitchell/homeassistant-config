#!/usr/bin/env python3

"""
Usage:
    zm.py --clear MONITOR [options]
    zm.py --get [options]
    zm.py --list [options]
    zm.py --set STATE [options]
    zm.py --trigger MONITOR [options]

Options:
    -h, --help                     Show this help message and exit.
    --config CONFIG                Configuration file. Defaults to secrets.yaml. [default: /home/homeassistant/.homeassistant/secrets.yaml]
    --log-level                    Logging level during execution. Available options: DEBUG, INFO, WARNING, ERROR, CRITICAL. [default: ERROR]
    -c MONITOR, --clear MONITOR    Clear an alarm on the monitor.
    -t MONITOR, --trigger MONITOR  Trigger an alarm on the monitor.
    -g, --get                      Get the current state.
    -l, --list                     List all states.
    -s STATE, --set STATE          Change the current state.
"""

import json, logging, os, traceback, yaml
import pyzm.api as zmapi
from docopt import docopt

def main():
    api_options = {
        'apiurl': cfg['zm_apiurl'],
        'portalurl': cfg['zm_portalurl'],
        'user': cfg['zm_username'],
        'password': cfg['zm_password'],
        'logger': None
    }
    try:
        zm = zmapi.ZMApi(options=api_options)
    except Exception as e:
        print('ERROR: {}'.format(str(e)))
        print(traceback.format_exc())
        exit(1)
    if arguments['--list'] or arguments['--get']:
        states = zm.states()
        for state in states.list():
            if arguments['--list']:
                print('State:{}[{}], active={}, details={}'.format(state.name(), state.id(), state.active(), state.definition()))
            else:
                if state.active():
                    print('State:{}[{}], active={}, details={}'.format(state.name(), state.id(), state.active(), state.definition()))
                    with open("/home/homeassistant/.homeassistant/.zm_state", "w") as outfile:
                        outfile.write(state.name())
        exit(0)
    elif arguments['--set'] != None:
        try:
            zm.set_state(state=arguments['--set'])
        except Exception as e:
            print('ERROR: {}'.format(str(e)))
            print(traceback.format_exc())
            exit(1)
        exit(0)
    elif arguments['--trigger'] != None or arguments['--clear'] != None:
        if arguments['--trigger'] != None:
            mname = arguments['--trigger']
        else:
            mname = arguments['--clear']
        try:
            ms = zm.monitors()
            m = ms.find(name=mname)
        except Exception as e:
            print('ERROR: {}'.format(str(e)))
            print(traceback.format_exc())
            exit(1)
        if arguments['--trigger']:
            m.arm()
        else:
            m.disarm()
        exit(0)
    else:
        print("Invalid option or usage.")
        exit(2)

if __name__ == "__main__":
    arguments = docopt(__doc__)

    # configure logger
    logging.basicConfig(level="WARNING", format='%(asctime)s %(filename)s:%(funcName)s - [%(levelname)s] %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
    logger = logging.getLogger()
    logger.setLevel(arguments['--log-level'])
    logger.debug(arguments)

    # read in YAML configuration file
    if "~" in arguments['--config']:
        pattern = re.compile('~')
        arguments['--config'] = pattern.sub(os.path.expanduser("~"), arguments['--config'])
    if not os.path.exists(arguments['--config']):
        logger.error("Specified configuration file does not exist!")
        exit(1)
    with open(arguments['--config'], 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
        logger.debug(cfg)

    main()

    exit(0)
